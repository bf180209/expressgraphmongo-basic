// denke dass es so funktionieren wird

const express = require('express')
const passport = require('passport')

const upload = require('../middleware/upload')
const controller = require('../controllers//UploadController')

const router = express.Router()

router.post('/', upload.single('file'), passport.authenticate('jwt', { session: false }), controller.store)
router.patch('/:id', upload.single('file'), passport.authenticate('jwt', { session: false }), controller.update)
router.delete('/:file', passport.authenticate('jwt', { session: false }), controller.destroy)


module.exports = router


// es gibt zwei möglichkeiten wie man auf URL zugreift

// 1 params
// router.get(route + '/:id', controller.show)
// req.params.id

// 2 query
// localhost:5000/order?limit=30
// req.query.limit