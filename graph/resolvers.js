const Contact = require('../models/Contact')
module.exports = {
  hello: () => 'Hello world!',
  //--- Contact ------------------------
  indexContact: async (arg, req) => {
    const contacts = await Contact.find({})
    return contacts
  },

  showContact: async ({ _id }, req) => {
    console.log(_id)
    const contact = await Contact.findById(_id)
    return contact
  },

  storeContact: async (args, context) => {
    var newContact = new Contact({
      name: args.name,
      // author: args.author
    })

    var err = await newContact.save()

    if (err) return err
    return newContact
  },
  updateContact: async ({ _id, name }, req) => {
    const contact = await Contact.findOneAndUpdate({ _id }, { $set: { name } })
    return contact
  },
  destroyContact: async (args, context) => {
    var doc = await Contact.findOneAndRemove({
      _id: args._id
    })
    return doc
  }
};
