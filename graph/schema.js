var { buildSchema } = require('graphql');

module.exports = buildSchema(`
type User {
  _id: ID
  username: String! 
  email: String!
  password: String!
}

type Contact {
    _id: ID!
    name: String!
  }

  type Query {
    #--Contact-----------------------------------
    indexContact: [Contact]
    showContact(_id: ID!): Contact
  }

  type Mutation {
    #--Contact-----------------------------------
    storeContact(name: String!): Contact!
    updateContact(_id:ID name: String): Contact!
    destroyContact(_id: ID!): Contact!
  }
    `);
