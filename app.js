const express = require('express')
const mongoose = require('mongoose')
const express_graphql = require('express-graphql');
const bodyParser = require('body-parser')
const passport = require('passport')

const authRoutes = require('./routes/auth')
const uploadRoutes = require('./routes/uploadRoute')
const keys = require('./config/keys')

// Import typeDefs and resolvers
const schema = require('./graph/schema');
const rootValue = require("./graph/resolvers");

const app = new express()

mongoose.connect(keys.mongoURL, { useNewUrlParser: true })
    .then(() => console.log('DB connected'))
    .catch(e => console.log(e))

app.use(passport.initialize())
require('./middleware/passport')(passport)

app.use(require('morgan')('dev'))
app.use('/uploads', express.static('uploads')) // server gibt ordner frei

app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())

app.use(require('cors')())

app.use('/graphql', express_graphql({
    schema,
    rootValue,
    graphiql: true
}));
app.use('/api/upload', uploadRoutes)

//http://localhost:5000/api/auth/login
//http://localhost:5000/api/auth/register
app.use('/api/auth', authRoutes)

module.exports = app