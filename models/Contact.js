const mongoose = require('mongoose')
const ObjectId = mongoose.Types.ObjectId;
ObjectId.prototype.valueOf = function () {
    return this.toString();
};
const Schema = mongoose.Schema
const contactSchema = new Schema({
    nr: {
        type: Number,
    },
    name: {
        type: String,
    },
    address: {
        type: String,
    },
    email: {
        type: String,
    },
    phone: {
        type: String,
    },
    note: {
        type: String,
    },
    user: {
        ref: 'users',
        type: Schema.Types.ObjectId
    },
    created_at: {
        type: Date,
        default: Date.now
    }

})

module.exports = mongoose.model('Contact', contactSchema)