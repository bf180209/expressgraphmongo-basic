
const fs = require('fs')

module.exports.store = function (req, res) {

    res.status(200).json({
        file: req.file.path
    })
}

module.exports.update = function (req, res) {

    res.status(200).json({
        method: 'update'
    })
}

module.exports.destroy = function (req, res) {
    //https://flaviocopes.com/how-to-remove-file-node/
    // delete http://localhost:5000/api/upload/20181130-220321_525-erfolg_spruch.jpg
    const file = req.params.file
    const path = 'uploads/' + req.user._id + '/' + file
    console.log(path)
    fs.unlink(path, (err) => {
        if (err) {
            console.error(err)
        }
        //file removed
    })

    res.status(200).json({
        suscess: file
    })
}