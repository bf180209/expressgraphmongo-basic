const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')

const User = require('../models/User')
const keys = require('../config/keys')
const errorHandler = require('../utils/errorHandler')

module.exports.login = async function (req, res) {

    const candidate = await User.findOne({ email: req.body.email }).populate('setting').populate('company')

    if (candidate) {
        // check password
        const passwordResult = bcrypt.compareSync(req.body.password, candidate.password)

        if (passwordResult) {

            //generate Token
            const token = jwt.sign({
                email: candidate.email,
                userId: candidate._id
            }, keys.jwt, { expiresIn: 60 * 60 * 24 * 30 })



            res.status(200).json({
                "user": candidate,
                "token_type": "Bearer",
                access_token: `${token}`
            })

        } else {

            res.status(401).json({
                message: 'Password falsch'
            })
        }

    } else {

        res.status(404).json({
            message: 'Email nicht gefunden'
        })
    }

}

module.exports.register = async function (req, res) {

    //email -password
    const candidat = await User.findOne({ email: req.body.email })

    if (candidat) {
        res.status(409).json({
            message: 'Email schon verwendet'
        })

    } else {

        // hash password
        const salt = bcrypt.genSaltSync(10)
        const password = req.body.password

        try {

            const user = await new User({
                username: req.body.username,
                email: req.body.email,
                password: bcrypt.hashSync(password, salt),
            }).save()

            res.status(201).json(user)

        } catch (e) {
            errorHandler(res, e)
        }



    }
}


// module.exports.register = async function (req, res) {
//
//     //email -password
//     const candidat = await User.findOne({email: req.body.email})
//
//     if (candidat) {
//             res.status(409).json({
//                 message: 'Email schon verwendet'
//             })
//
//     } else {
//
//         // hash password
//         const salt = bcrypt.genSaltSync(10)
//         const password = req.body.password
//
//     try {
//
//         const user = await  new User({
//             username: req.body.username,
//             email: req.body.email,
//             password: bcrypt.hashSync(password, salt),
//         }).save()
//
//         if (req.query.company && req.query.company.length == 24) {
//             var companyId = req.query.company
//         } else {
//             const company = await new Company({
//                 user: user._id
//             }).save()
//
//             var companyId = company._id
//         }
//
//         const setting = await new Setting({
//             user: user._id,
//             company:[companyId]
//         }).save()
//
//             res.status(201).json(user)
//
//      } catch (e) {
//         errorHandler(res, e)
//     }
//
//
//
//     }
// }