const multer = require('multer')
const moment = require('moment')

const ExtractJwt = require('passport-jwt').ExtractJwt
const jwtVerify = require('jsonwebtoken').verify
const keys = require('../config/keys')
const token = ExtractJwt.fromAuthHeaderAsBearerToken()
const fs = require('fs');



const storage = multer.diskStorage({
    // cb = callback
    destination(req, file, cb) {
        // user.userId
        const user = jwtVerify(token(req), keys.jwt);
        path = 'uploads/' + user.userId + '/'
        fs.exists('./uploads/' + user.userId + '/', (exists) => {
            if (!exists) {
                fs.mkdir('./uploads/' + user.userId + '/', { recursive: true }, (err) => {
                    if (err) throw err;
                })
            }
        });

        cb(null, path)
    },
    filename(req, file, cb) {
        const date = moment().format('YYYYMMDD-HHmmss_SSS')
        cb(null, `${date}-${file.originalname}`)
    }
})

const fileFilter = (req, file, cb) => {
    if (file.mimetype === 'image/png' || file.mimetype === 'image/jpeg') {
        cb(null, true)
    } else {
        cb(null, false)
    }
}

const limits = {
    fileSize: 1024 * 1024 * 5
}


module.exports = multer({ storage, fileFilter, limits })